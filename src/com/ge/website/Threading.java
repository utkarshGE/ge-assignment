package com.ge.website;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Threading {
	
	public static void callThreads(List<String> urlsTobeCrawled,boolean isBaseURL){
			 		    
		Collection<Callable<String>> tasks = new ArrayList<>();
		    for(String url : urlsTobeCrawled){
		         tasks.add(new SurfPages(url,isBaseURL));    // add URL's for executor framework
		    }
		    try{
		    ExecutorService executor = Executors.newCachedThreadPool();
		    List<Future<String>> results = executor.invokeAll(tasks);
		    for(Future<String> result : results){
		    	String threadInfo = result.get();
		      System.out.println(threadInfo);
		    }
		    executor.shutdown(); 
		    }
		    catch(Exception e){
		    e.printStackTrace();
		    }}}