package com.ge.website;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class ParseWebsite {
	
	public static void main(String args[]){
		
		Threading threads = new Threading();
		 List<String> urlsTobeCrawled =new ArrayList<String>();
		 urlsTobeCrawled.add("https://clearlydecoded.com");   // add number of web sites for which crawling needs to be done
		 // urlsTobeCrawled.add("https://timesofindia.com");
		 
		 		 
		 Iterator<String> itr = urlsTobeCrawled.iterator();
		 while(itr.hasNext()){
		  threads.callThreads(Arrays.asList(itr.next()),Boolean.TRUE);   // will pick one web site at a time. 
		  SurfPages.finalCount();
		 }
		System.out.println("**********************    Program completed    ********************************************");
		}
	}