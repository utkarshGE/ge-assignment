package com.ge.website;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SurfPages implements Callable<String>{
	
	private String url=null;

	static Map<String,Boolean>  success =new ConcurrentHashMap<String,Boolean>();
	static Set<String>  skipped =new HashSet<String>();
	static Set<String>  error =new HashSet<String>();
		
	StringBuffer updatedURL=new StringBuffer();
	 static String baseURL;
	
	SurfPages(String url,boolean isBaseURL){
		this.url=url;
		if(isBaseURL){
		SurfPages.baseURL=url;
	 }}

	@Override
	public String call() throws Exception {
		return surfWebsite(url);
	}
	
	public String surfWebsite(String urlToParse) {
				String singleUrl=null;
				Document doc;
			
				try {
				doc = Jsoup.connect(urlToParse).get();  // used third party library Jsoup file for parsing URL's
				Elements links = doc.select("a[href]");  // Will read page links  
		        for (Element link : links) {
		        	singleUrl=link.attr("href");
		        	
		            	if(!singleUrl.startsWith("https") || (!singleUrl.startsWith("http"))){  // Will make short URL's proper URL's 
	        	           	updatedURL.setLength(0);
	        	        	updatedURL.append(baseURL+"/"+singleUrl);
	        	        	singleUrl=updatedURL.toString();
	        	        	
	        	        }if(!singleUrl.contains(baseURL)){  // will filter and remove social web site and third party web sites URL
		            		continue;
		            	}if(success.containsKey(singleUrl)){  // If URL is already parsed will come in skipped
		            		skipped.add(singleUrl);
		              	}else{
			            	if(getHttpCode(singleUrl)==404){ // will check single URL HTP response code 
			            		error.add(singleUrl);
			            		continue;
			            	}
		   		         	success.put(singleUrl,Boolean.FALSE); // If URL is first time parsed will come in success set with URL parsed flag as false
		            	}}
		        
		        if(success.containsValue(Boolean.FALSE)){
		        	List<String> linkUrls = new ArrayList<String>();
		        	success.forEach((key, value) -> {
		        	    if (value==Boolean.FALSE) {
		        	        success.put(key, Boolean.TRUE);
		        	        linkUrls.add(key);      // will add URL's and pass for multi-threading execution
		        	    }});
				Threading.callThreads(linkUrls, Boolean.FALSE);  // calls code for multi-threading
		       }} catch (HttpStatusException e) {  // if website throw any other HTTP error code (500,401) it will come in console logs 
		        	if(e.getStatusCode()==404)
		        		error.add(e.getUrl());
		        	e.printStackTrace();
		        	}
				catch (Exception e) {
		        		e.printStackTrace();
				}
				return "Thread completed = "+Thread.currentThread().getName();
	}
	
		private int getHttpCode(String urlLink) {
			URL url;
			try {
				url = new URL(urlLink);
				HttpURLConnection connection = (HttpURLConnection)url.openConnection();
				connection.setRequestMethod("GET");
				connection.connect();
				return connection.getResponseCode();
			} catch (Exception e) {}
			return 404;     
	}
		
		// will publish parsing result in console
		public static void finalCount(){
			System.out.println("==============================================================");
			System.out.println("URL parsed result = "+baseURL);
			System.out.println("Success = "+success.size()+" Skipped = "+skipped.size()+" error = "+error.size());
			System.out.println("Success = "+success.keySet());
			System.out.println("skipped = "+skipped);
			System.out.println("error = "+error);
			System.out.println("==============================================================");
			success.clear();
			skipped.clear();
			error.clear();
			System.gc();
		}}