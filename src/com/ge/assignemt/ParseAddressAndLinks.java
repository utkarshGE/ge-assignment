package com.ge.assignemt;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.RecursiveTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ParseAddressAndLinks extends RecursiveTask<Long>{
    
	private static final long serialVersionUID = 9132278916989879354L;
	
	static Map<String,String>  success =new ConcurrentHashMap<String,String>();
	static Map<String,String>  skipped =new ConcurrentHashMap<String,String>();
	static Map<String,String>  error =new ConcurrentHashMap<String,String>();
	
	private static String jsonData=null;
	private static String  pageNumber=null;
	
	ParseAddressAndLinks(String pageNumber,String filePath){    	
	    	try {
	    		this.jsonData= new String(Files.readAllBytes(Paths.get(filePath)));
	    		this.pageNumber=pageNumber;
			} catch (IOException e) {
				e.printStackTrace();
		}}
	
		protected Long compute() {
       	success.put(this.pageNumber, "");     // for first time since page is getting parsed, its number is placed in "success" map
    	try {
    		this.filterData(pageNumber);      
		} catch (JSONException e) {
			e.printStackTrace();
		}
    	System.out.println("=========== Result ===============");    	
    	System.out.println("success = "+success.keySet());
    	System.out.println("skipped = "+skipped.keySet());
    	System.out.println("error = "+error.keySet());
		return null;
    }

    private String parseAddress(String pageNuber) throws JSONException{  // function will parse json data from pdf file
    	String address=null;
    	
    	JSONObject obj = new JSONObject(jsonData);
        JSONArray arr = obj.getJSONArray("pages");
        for (int i = 0; i < arr.length(); i++) {
        	address=arr.getJSONObject(i).getString("address");
        	if(address.equals(pageNuber)){
            	return arr.getJSONObject(i).getString("links");
          }}
		return null;
    }

    private void filterData(String pageNumber) throws JSONException{   // function will filter data as per address and links given in a json file
    	String pages= parseAddress(pageNumber);
      	if(pages!=null){
    	for(String page:pages.split(",")){
    		page=page.replace("[", "").replace("\"", "").replace("]","");
    		
    		if(!success.containsKey(page) && !error.containsKey(page)){
    			success.put(page, "");
    			filterData(page);
    	}else{
    			skipped.put(page, "");
    		}}}else{
    		error.put(pageNumber, "");
    		success.remove(pageNumber);   
    		skipped.remove(pageNumber);
    	}}}